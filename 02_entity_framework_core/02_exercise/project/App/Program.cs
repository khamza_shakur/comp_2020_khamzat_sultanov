﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Utils;

namespace App
{
    [ExcludeFromCodeCoverage]
    public class ProjectContext : DbContext
    {
        public DbSet<Project>? Projects { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=blog.db");
        }
    }
    internal static class Program
    {
        private static void Main()
        {
            using (var db = new ProjectContext())
            {
                var project = new Project
                {
                    Name = "Xilinx", 
                    Description = "Is the inventor of the FPGA, programmable SoCs.", 
                    CreationDate = DateTime.Now,
                    Uri = new Uri("https://www.xilinx.com/")
                };
                db.Projects?.Add(project);
                db.SaveChanges();
            }

            using (var db = new ProjectContext())
            {
                var projects = db.Projects
                    .OrderBy(p => p.Name)
                    .ToList();

                foreach (var project in projects)
                {
                    Console.WriteLine($"{project.Name} --- {project.Description} --- {project.CreationDate} --- {project.Uri}");
                }
            } 
        }
    }
}