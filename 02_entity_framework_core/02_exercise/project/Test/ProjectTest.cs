using System;
using Utils;
using Xunit;

namespace Test
{
    public class ProjectTest
    {
        [Fact]
        public void CanCreateProject()
        {
            const int id = 1;
            const string name = "N";
            const string description = "D";
            var data = DateTime.Today;
            
             var uri = new Uri("http://example.com");

            var project = new Project
            {
                Id = id, 
                Name = name, 
                Description = description, 
                CreationDate = data,
                
                Uri = uri
            };

            Assert.Equal(id, project.Id);
            Assert.Equal(name, project.Name);
            Assert.Equal(description, project.Description);
            Assert.Equal(data, project.CreationDate);
            
             Assert.Equal(uri, project.Uri);
        }
    }
}