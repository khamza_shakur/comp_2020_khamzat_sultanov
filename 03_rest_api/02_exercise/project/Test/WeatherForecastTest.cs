using System;
using System.Collections.Generic;
using System.Linq;
using App.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using Utils;
using Xunit;

namespace Test
{
    public class WeatherForecastTest
    {
        [Fact]
        public void SampleTest()
        {
            var wf = new WeatherForecast();
            wf.TemperatureC = 0;
            wf.Date = DateTime.MaxValue;
            wf.Summary = "Testowa Pogoda";

            var tempF = 32;
            Assert.Equal( wf.TemperatureF, tempF );
            Assert.Equal(DateTime.MaxValue, wf.Date);
            Assert.Equal("Testowa Pogoda", wf.Summary);
        }
        [Fact]
        public void GetTest()
        {
            var mock = new Mock<ILogger<WeatherForecastController>>();
            var weatherForecast = new WeatherForecastController(mock.Object);
            IEnumerable<WeatherForecast> arr = weatherForecast.Get();
            
            mock.Verify(exp => exp.Log(It.IsAny<LogLevel>(), It.IsAny<EventId>(),
                It.Is<It.IsAnyType>((o, t) => string.Equals("WeatherForecastController.Get",
                    o.ToString(), StringComparison.InvariantCultureIgnoreCase)),
                It.IsAny<Exception>(),
                It.Is<Func<It.IsAnyType,
                    Exception, 
                    string>>((v, t) => true)));
            
            Assert.Equal(5,arr.Count());
            Assert.InRange(arr.ElementAt(0).TemperatureC,-20, 55);
            Assert.InRange(arr.ElementAt(1).TemperatureF,-4, 131);
            Assert.InRange(arr.ElementAt(2).TemperatureC,-20, 55);
            Assert.InRange(arr.ElementAt(3).TemperatureF,-4, 131);
            Assert.InRange(arr.ElementAt(4).TemperatureC,-20, 55);
        }
    }
}