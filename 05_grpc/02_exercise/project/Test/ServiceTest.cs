using Castle.Core.Logging;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Moq;
using Proto;
using Server;
using Xunit;

namespace Test
{
    public class ServiceTest
    {
        [Fact]
        public void GreeterTest()
        {
            var req = new HelloRequest();
            req.Name = "John";

            var serverCallContextMock = new Mock<ServerCallContext>();
            var loggerMock = new Mock<ILogger<GreeterService>>();
            var greeterService = new GreeterService(loggerMock.Object);
            var result = greeterService.SayHello(req, serverCallContextMock.Object).Result;
            
            Assert.IsType<HelloRequest>(req);
            Assert.Equal("Hello John", result.Message);
        }
    }
}