using Blazor.Pages;
using Xunit;

namespace Test
{
    public class CounterTest
    {
        [Fact]
        public void CounterZero()
        {
            var counter = new Counter();
            
            Assert.Equal(0, counter._currentCount);
            
            counter.IncrementCount();
            counter.IncrementCount();
            
            Assert.Equal(2, counter._currentCount);
        }

        [Fact]
        public void CounterIncrement()
        {
            var counter = new Counter();
            counter.IncrementCount();
            counter.IncrementCount();
            
            Assert.Equal(2, counter._currentCount);
        }
    }
}