using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using Moq;
using Blazor.Data;
using Xunit;

namespace Test
{
    public class WeatherForecastTest
    {
        [Fact]
        public void SampleTest()
        {
            var wf = new WeatherForecast();
            wf.TemperatureC = 0;
            wf.Date = DateTime.MaxValue;
            wf.Summary = "Testowa Pogoda";

            var tempF = 32;
            Assert.Equal( wf.TemperatureF, tempF );
            Assert.Equal(DateTime.MaxValue, wf.Date);
            Assert.Equal("Testowa Pogoda", wf.Summary);
        }
    }
}