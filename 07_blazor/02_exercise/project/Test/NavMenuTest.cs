using Blazor.Shared;
using Xunit;

namespace Test
{
    public class NavMenuTest
    {
        [Fact]
        public void CollapseBool()
        {
            var navMenu = new NavMenu();
            
            Assert.True(navMenu._collapseNavMenu);
        }

        [Fact]
        public void NavMenuCssClass()
        {
            var navMenu = new NavMenu();
            
            Assert.Same("collapse", navMenu.NavMenuCssClass);
        }

        [Fact]
        public void ToggleNavMenu()
        {
            var navMenu = new NavMenu();
            
            Assert.True(navMenu._collapseNavMenu);

            navMenu.ToggleNavMenu();
            
            Assert.False(navMenu._collapseNavMenu);
        }
    }
}