using System;
using Gui.ViewModels;
using Xunit;

namespace Test
{
    public class MainWindowViewModelTest
    {
        [Fact]
        public void Name()
        {
            var mainWindowViewModel = new MainWindowViewModel();
            const string name = "Test";
            mainWindowViewModel.Name = name;
            
            Assert.NotNull(mainWindowViewModel);
            Assert.NotEmpty(mainWindowViewModel.Name);
            Assert.Equal(mainWindowViewModel.Name, name);
        }

        [Fact]
        public void Greeting()
        {
            var mainWindowViewModel = new MainWindowViewModel();
            const string name = "NameTest";
            
            mainWindowViewModel.Name = name;
            mainWindowViewModel.OnClick.Execute().Subscribe();

            Assert.Equal($"Hello {name}!", mainWindowViewModel.Greeting);
        }

    }
}