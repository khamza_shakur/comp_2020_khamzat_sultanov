﻿using System.Reactive;
using ReactiveUI;

namespace Gui.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        private string _name = "";
        private string _greeting = "";

        public string Name
        {
            get => _name;
            set => this.RaiseAndSetIfChanged(ref _name, value);
        }

        public string Greeting
        {
            get => _greeting;
            set => this.RaiseAndSetIfChanged(ref _greeting, value);
        }

        public ReactiveCommand<Unit, Unit> OnClick { get; }
        public MainWindowViewModel()
        {
            OnClick = ReactiveCommand.Create(() =>
            {
                Greeting = $"Hello {_name}!";
                Name = "";
            });
        }
    }
}