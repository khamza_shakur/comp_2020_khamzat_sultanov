using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Xunit;

namespace Test
{
    internal interface IFoo
    {
        int Test();
    }

    internal class Foo : IFoo
    {
        public int Test()
        {
            return 7;
        }
    }

    internal class Bar
    {
        private readonly IFoo _foo;

        public Bar(IFoo foo)
        {
            _foo = foo;
        }

        public int Test()
        {
            return _foo.Test();
        }
    }

    public class DependencyInjectionTest
    {
        [Fact]
        public void HasSomeTypesForTesting()
        {
            var foo = new Foo();
            var bar = new Bar(foo);
            Assert.Equal(7, bar.Test());
        }

        [Fact]
        public void CreatingBarTest()
        {
            var foo = new Foo();
            var bar = new Bar(foo);
            
            Assert.NotNull(bar);
        }
        
        [Fact]
        public void AddSingletonTest()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddSingleton<IFoo, Foo>();
            serviceCollection.AddSingleton<Bar>();
            var serviceProvider = serviceCollection.BuildServiceProvider();
            var bar1 = serviceProvider.GetService<Bar>();
            var bar2 = serviceProvider.GetService<Bar>();

            Assert.Same(bar1, bar2);
        }
        
        [Fact]
        public void AddTransientTest()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddTransient<IFoo, Foo>();
            serviceCollection.AddTransient<Bar>();
            var serviceProvider = serviceCollection.BuildServiceProvider();

            var bar1 = serviceProvider.GetService<Bar>();
            var bar2 = serviceProvider.GetService<Bar>();
            
            Assert.NotSame(bar1, bar2);
        }

        [Fact]
        public void AddScopedTest()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddScoped<IFoo, Foo>();
            serviceCollection.AddScoped<Bar>();
            var serviceProvider = serviceCollection.BuildServiceProvider();

            var bar1 = serviceProvider.GetService<Bar>();
            var bar2 = serviceProvider.GetService<Bar>();
            
            Assert.Same(bar1, bar2);

            using (var scope = serviceProvider.CreateScope())
            {
                serviceCollection.AddScoped<IFoo, Foo>();
                serviceCollection.AddScoped<Bar>();
                serviceProvider = serviceCollection.BuildServiceProvider();
                bar1 = serviceProvider.GetService<Bar>();
            }
            
            using (var scope = serviceProvider.CreateScope())
            {
                serviceCollection.AddScoped<IFoo, Foo>();
                serviceCollection.AddScoped<Bar>();
                serviceProvider = serviceCollection.BuildServiceProvider();
                bar2 = serviceProvider.GetService<Bar>();
            }
            
            Assert.NotSame(bar1, bar2);
        }
    }
}