using System;
using System.IO;
using System.Threading.Tasks;
using Castle.Core.Logging;
using Microsoft.Extensions.Logging;
using Moq;
using Utils;
using Xunit;
using ILogger = Castle.Core.Logging.ILogger;

namespace Test
{
    public class DemoTest
    {
        /*[Fact]
        public void Run()
        {
            var mock = new Mock<TextWriter>();
            Console.SetOut(mock.Object);

            const string name = "John";
            mock.Setup(tw => tw.WriteLine($"Hello {name}!"));
            
            var demo = new Demo(name);
            demo.Run();
        }*/
        [Fact]
        public void Run()
        {
            var mock = new Mock<ILogger<Demo>>();

            ILogger<Demo> logger = mock.Object;
            var demo = new Demo(logger);
            demo.Run("John");
            
            mock.Verify(
                x => x.Log(
                    It.IsAny<LogLevel>(),
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((o, t) => string.Equals("Hello John!", o.ToString(), StringComparison.InvariantCultureIgnoreCase)),
                    It.IsAny<Exception>(),
                    It.Is<Func<It.IsAnyType, Exception, string>>((v, t) => true)));
        }
    }
}