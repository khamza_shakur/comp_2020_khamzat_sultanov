using App.Models;
using Xunit;

namespace Test
{
    public class ErrorViewTest
    {
        [Fact]
        public void RequestedIdTest()
        {
            var errorView = new ErrorViewModel();
            Assert.False(errorView.ShowRequestId);
            errorView.RequestId = "200";
            
            Assert.Equal("200", errorView.RequestId);
            Assert.True(errorView.ShowRequestId);
        }
    }
}