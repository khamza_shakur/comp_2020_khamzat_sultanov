using System;
using System.Diagnostics;
using App.Controllers;
using App.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace Test
{
    public class HomeControllerTest : IDisposable
    {
        private readonly Activity _unitTestActivity = new Activity("ErrTest").Start();

        public void Dispose()
        {
            _unitTestActivity.Stop();
        }
        
        [Fact]
        public void IndexTest()
        {
            var mock = new Mock<ILogger<HomeController>>();
            var hc = new HomeController(mock.Object);

            var res = hc.Index();
            Assert.IsType<ViewResult>(res);
        }

        [Fact]
        public void PrivacyTest()
        {
            var mock = new Mock<ILogger<HomeController>>();
            var hc = new HomeController(mock.Object);

            var res = hc.Privacy();
            Assert.IsType<ViewResult>(res);
        }

         [Fact]
         public void ErrorTest()
         {
             var mock = new Mock<ILogger<HomeController>>();
             var hc = new HomeController(mock.Object);
 
             var res = hc.Error();
             var okRes = res as ViewResult;
             
             Assert.IsType<ErrorViewModel>(okRes?.Model);
         }
    }
}